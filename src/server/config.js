const dotenv = require("dotenv");
dotenv.config();

module.exports = {
  port: process.env.PORT || 3000,
  mysql_host: process.env.MYSQL_HOST,
  mysql_user: process.env.MYSQL_USER,
  mysql_password: process.env.MYSQL_PASSWORD,
  mysql_database_name: process.env.MYSQL_DATABASE_NAME,
};
