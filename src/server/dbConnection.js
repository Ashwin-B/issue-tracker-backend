const mysql = require("mysql");
const {
  mysql_database_name,
  mysql_host,
  mysql_password,
  mysql_user,
} = require("./config");

const connection = mysql.createPool({
  host: mysql_host,
  user: mysql_user,
  password: mysql_password,
  database: mysql_database_name,
});

module.exports = connection;
