const express = require("express");
const Joi = require("joi");
const { dbQuery } = require("../createDbTable");
const connection = require("../dbConnection");

const router = express.Router();

const commentSchema = Joi.object({
  issue_id: Joi.number().integer(),
  comment: Joi.string().min(1).max(400),
});

router.get("/:id", async (req, res, next) => {
  const getSelectedComments = `SELECT * from comments WHERE issue_id = ${connection.escape(
    req.params.id
  )}`;
  try {
    const selectedComments = await dbQuery(connection, getSelectedComments);
    if (selectedComments.length === 0) {
      res.json({ error: "Cannot find the requested project." });
    } else {
      res.json(selectedComments);
    }
  } catch (error) {
    next(error);
  }
});

router.post("/", async (req, res, next) => {
  const insertingComments = `INSERT INTO comments (issue_id, comment) VALUES (${connection.escape(
    req.body.issue_id
  )},${connection.escape(req.body.comment)})`;
  const getinsertedComment = `SELECT * FROM comments WHERE id = LAST_INSERT_ID()`;

  const { error } = commentSchema.validate(req.body);

  try {
    if (error) {
      res.json({ error: error.details[0].message });
    } else {
      await dbQuery(connection, insertingComments);
      const insertedComment = await dbQuery(connection, getinsertedComment);
      res.json(insertedComment);
    }
  } catch (error) {
    next(error);
  }
});

router.patch("/:id", async (req, res, next) => {
  const updateSelectedComment = `UPDATE comments SET comment=${connection.escape(
    req.body.comment
  )} WHERE id = ${connection.escape(req.params.id)}`;

  const { error } = commentSchema.validate(req.body);

  try {
    if (error) {
      res.json({ error: error.details[0].message });
    } else {
      const updatedComment = await dbQuery(connection, updateSelectedComment);
      if (updatedComment.affectedRows === 0) {
        res.json({
          error: "Comment cannot be updated.",
        });
      } else {
        res.json(updatedComment);
      }
    }
  } catch (error) {
    next(error);
  }
});

router.delete("/:id", async (req, res, next) => {
  const deleteSelectedComment = `DELETE FROM comments WHERE id = ${connection.escape(
    req.params.id
  )}`;
  try {
    const deletedComment = await dbQuery(connection, deleteSelectedComment);
    if (deletedComment.affectedRows === 0) {
      res.json({ error: "Cannot delete the requested comment." });
    } else {
      res.json(deletedComment);
    }
  } catch (error) {
    next(error);
  }
});

module.exports = router;
