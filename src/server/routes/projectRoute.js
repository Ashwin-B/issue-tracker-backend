const express = require("express");
const Joi = require("joi");
const { dbQuery } = require("../createDbTable");
const connection = require("../dbConnection");

const router = express.Router();

const projectNameSchema = Joi.object({
  name: Joi.string().min(3).max(30).required(),
});

router.get("/", async (req, res, next) => {
  const getAllProjects = "SELECT * from projects";
  try {
    const projectList = await dbQuery(connection, getAllProjects);
    res.json(projectList);
  } catch (error) {
    next(error);
  }
});

router.get("/:id", async (req, res, next) => {
  const getSelectedProject = `SELECT * from projects WHERE id = ${connection.escape(
    req.params.id
  )}`;
  try {
    const selectedProject = await dbQuery(connection, getSelectedProject);
    if (selectedProject.length === 0) {
      res.json({ error: "Cannot find the requested project." });
    } else {
      res.json(selectedProject);
    }
  } catch (error) {
    next(error);
  }
});

router.post("/", async (req, res, next) => {
  const insertingProject = `INSERT INTO projects (name) VALUES (${connection.escape(
    req.body.name
  )})`;
  const getinsertedProject = `SELECT * FROM projects WHERE id = (SELECT LAST_INSERT_ID())`;
  const { error } = projectNameSchema.validate(req.body);

  try {
    if (error) {
      res.json({ error: error.details[0].message });
    } else {
      await dbQuery(connection, insertingProject);
      const insertedProject = await dbQuery(connection, getinsertedProject);
      res.json(insertedProject);
    }
  } catch (error) {
    next(error);
  }
});

router.patch("/:id", async (req, res, next) => {
  const updateSelectedProject = `UPDATE projects SET name=${connection.escape(
    req.body.name
  )} WHERE id = ${connection.escape(req.params.id)}`;

  const { error } = projectNameSchema.validate(req.body);

  try {
    if (error) {
      res.json({ error: error.details[0].message });
    } else {
      const updatedProject = await dbQuery(connection, updateSelectedProject);
      if (updatedProject.affectedRows === 0) {
        res.json({
          error: "Project name cannot be updated for the requested project.",
        });
      } else {
        res.json(updatedProject);
      }
    }
  } catch (error) {
    next(error);
  }
});

router.delete("/:id", async (req, res, next) => {
  const deleteSelectedProject = `DELETE FROM projects WHERE id = ${connection.escape(
    req.params.id
  )}`;
  try {
    const deletedProject = await dbQuery(connection, deleteSelectedProject);
    if (deletedProject.affectedRows === 0) {
      res.json({ error: "Cannot delete the requested project." });
    } else {
      res.json(deletedProject);
    }
  } catch (error) {
    next(error);
  }
});

module.exports = router;
