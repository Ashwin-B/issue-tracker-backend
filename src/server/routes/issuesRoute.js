const express = require("express");
const Joi = require("joi");
const { dbQuery } = require("../createDbTable");
const connection = require("../dbConnection");

const router = express.Router();

const issueSchema = Joi.object({
  project_id: Joi.number().integer(),
  issue_title: Joi.string().min(3).max(40),
  issue: Joi.string().min(3).max(400),
  status: Joi.string().min(3).max(10),
});

router.get("/:id", async (req, res, next) => {
  const getSelectedIssue = `SELECT * from issues WHERE project_id = ${connection.escape(
    req.params.id
  )}`;

  try {
    const selectedIssue = await dbQuery(connection, getSelectedIssue);
    if (selectedIssue.length === 0) {
      res.json({ error: "Cannot find the requested issue." });
    } else {
      res.json(selectedIssue);
    }
  } catch (error) {
    next(error);
  }
});

router.post("/", async (req, res, next) => {
  const insertingIssue = `INSERT INTO issues (project_id,issue_title,issue, status) VALUES (${connection.escape(
    req.body.project_id
  )},${connection.escape(req.body.issue_title)} ,${connection.escape(
    req.body.issue
  )}, ${connection.escape(req.body.status)})`;

  const getInsertedIssue = `SELECT * FROM issues WHERE id = LAST_INSERT_ID()`;
  const { error } = issueSchema.validate(req.body);

  try {
    if (error) {
      res.json({ error: error.details[0].message });
    } else {
      await dbQuery(connection, insertingIssue);
      const insertedIssue = await dbQuery(connection, getInsertedIssue);
      res.json(insertedIssue);
    }
  } catch (error) {
    next(error);
  }
});

router.patch("/:id", async (req, res, next) => {
  const updateSelectedIssue = `UPDATE issues SET issue_title=${connection.escape(
    req.body.issue_title
  )}, issue=${connection.escape(req.body.issue)}, status=${connection.escape(
    req.body.status
  )} WHERE id = ${connection.escape(req.params.id)}`;

  const { error } = issueSchema.validate(req.body);

  try {
    if (error) {
      res.json({ error: error.details[0].message });
    } else {
      const updatedIssue = await dbQuery(connection, updateSelectedIssue);
      if (updatedIssue.affectedRows === 0) {
        res.json({
          error: "Issue cannot be updated for the requested id.",
        });
      } else {
        res.json(updatedIssue);
      }
    }
  } catch (error) {
    next(error);
  }
});

router.delete("/:id", async (req, res, next) => {
  const deleteSelectedIssue = `DELETE FROM issues WHERE id = ${connection.escape(
    req.params.id
  )}`;

  try {
    const deletedIssue = await dbQuery(connection, deleteSelectedIssue);
    if (deletedIssue.affectedRows === 0) {
      res.json({ error: "Cannot delete the requested issue." });
    } else {
      res.json(deletedIssue);
    }
  } catch (error) {
    next(error);
  }
});

module.exports = router;
