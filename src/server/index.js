const express = require("express");
const cors = require("cors");
const { port } = require("./config");
const connection = require("./dbConnection");
const { createProjectTable } = require("./createDbTable");
const projectRoute = require("./routes/projectRoute");
const issuesRoute = require("./routes/issuesRoute");
const commentsRoute = require("./routes/commentsRoute");

createProjectTable(connection).catch((error) => {
  console.error(error);
  return false;
});

const app = express();

app.use(cors());
app.use(express.json());

app.use("/api/projects", projectRoute);
app.use("/api/issues", issuesRoute);
app.use("/api/comments", commentsRoute);

app.use((req, res, next) => {
  res.status(404).send("Not found");
});

app.use((err, req, res, next) => {
  if (err) {
    res
      .status(500)
      .json({ error: "Cannot process your request. Please try again later." });
  } else {
    res.status(500).json({ error: "Internal Server error" });
  }
});

app.listen(port);
