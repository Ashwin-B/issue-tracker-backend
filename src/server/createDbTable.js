function dbQuery(connection, query) {
  return new Promise((resolve, reject) => {
    connection.query(query, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

async function createProjectTable(connection) {
  const dropCommentsTable = "DROP TABLE IF EXISTS comments";
  const dropProjectTable = "DROP TABLE IF EXISTS projects";
  const dropIssuesTable = "DROP TABLE IF EXISTS issues";

  const projectQuery =
    "CREATE TABLE projects(id INT NOT NULL AUTO_INCREMENT, name VARCHAR(40), PRIMARY KEY(id))";
  const issuesQuery =
    "CREATE TABLE issues(id INT NOT NULL AUTO_INCREMENT, project_id INT NOT NULL,issue_title VARCHAR(40) ,issue VARCHAR(400), status VARCHAR(20),PRIMARY KEY(id) ,FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE)";
  const commentsQuery =
    "CREATE TABLE comments(id INT NOT NULL AUTO_INCREMENT, issue_id INT NOT NULL, comment VARCHAR(400), PRIMARY KEY(id), FOREIGN KEY(issue_id) REFERENCES issues(id) ON DELETE CASCADE)";

  try {
    await dbQuery(connection, dropCommentsTable);
    await dbQuery(connection, dropIssuesTable);
    await dbQuery(connection, dropProjectTable);
    await dbQuery(connection, projectQuery);
    await dbQuery(connection, issuesQuery);
    await dbQuery(connection, commentsQuery);
  } catch (error) {
    throw error;
  }
}

module.exports = { createProjectTable, dbQuery };
